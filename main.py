from flask import Flask, request, render_template

app = Flask(__name__)

@app.route("/", methods=['GET', 'POST'])
def index_page():
    if request.method == 'POST':
        f = request.files['file']
        f.save('/Users/sutthikant.k/Documents/Habour.Space/Module5-python 2/ml05-final_project/dataset/data.csv')
    return render_template("main.html")

@app.route("/recieve-feature-target")
def recieve_feature_target():
    target = request.args.get("target")
    return {"result": target}

@app.route("/predict")
def predict():
    pass