# Data Processing Project
This is a final project  which tries to receive a set of data and do some analyze

## This project have 3 main functions
which are
1. Recieve a dataset file (required a .csv file)
2. Analyze data and show the output(mean, max, min, histogram of each features)
3. Recieve 2 target features and show the correlation chart between them

## The workflow of the project
1. Recieve the dataset(.csv file) and save it at the specific directory (/dataset/...)
2. Do a function in the class Readfile() to create a data frame using Pandas
3. Do another function called create_data_analyze_dictionary() to fetch data from the data frame and realign it in the from of dictionary
(it also generate a histogram picture and save it in the specific directory(/static/pictures/histogram/...) and named it with each features's name) for each features
4. Send it to render in the html page 
5. Recieve 2 target features and send it to compute in the function create_Corretion() and save it in the specific directory(/static/pictures/correlation/...) and named it by the specific combination of these features
6. Send it to render in the HTML page

## Limitation
- It requires to be run in the local user computer not universal level(for now)
- It can recieve only numuric dataset
