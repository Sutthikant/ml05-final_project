import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sn

import pathlib
x = str(pathlib.Path(__file__).parent.resolve())

class ReadFile:
    def __init__(self):
        self.df = pd.read_csv(x+'/dataset/data.csv')

    def create_data_analyze_dictionary(self):
        dic = {
            "data":[]
        }
        for col in list(self.df.columns):
            new_dic = {
                "feature": col,
                "mean": self.df[col].mean(),
                "max": self.df[col].max(),
                "min": self.df[col].min()
                }
            dic["data"].append(new_dic)
            self.create_Histogram(col)
        return dic
    
    def create_Histogram(self, feature):
        plt.hist(self.df[feature], bins = 20)
        plt.savefig(f'/Users/sutthikant.k/Documents/Habour.Space/Module5-python 2/ml05-final_project/pictures/histogram/{feature}.png')

    def create_Corretion(self, target1, target2):
        new_data = {target1: self.df[target1], target2: self.df[target2]}
        new_df = pd.DataFrame(new_data)
        corr_matrix = new_df.corr()
        sn.heatmap(corr_matrix, annot=True)
        plt.savefig(f'/Users/sutthikant.k/Documents/Habour.Space/Module5-python 2/ml05-final_project/pictures/correlation/{target1}_{target2}_corr.png')

a = ReadFile()
# print(list(a.df.columns))
print(a.create_data_analyze_dictionary())
a.create_Corretion("Pulse", "Duration")


'''
{
    "data_analyze": [
        {
            "feature": "f1"
            "mean": 123
            "max": 150
            "min": 110
            "histogram": "f1_histogram.png"
        }
    ]
}
'''